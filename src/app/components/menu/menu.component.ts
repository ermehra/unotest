import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoaderService, MenuService} from '../../services';
import {finalize} from 'rxjs/internal/operators';
import {ErrorService} from '../../services/error.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  locationId: number;
  categories: any;
  error: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _menu: MenuService,
              private _spinner: LoaderService,
              private _errorService: ErrorService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this._spinner.display(true);
      this._errorService.showErrorMessage(null);
    });

    this.locationId = parseInt(this._activatedRoute.snapshot.paramMap.get('id'));

    this._menu.getMenu(this.locationId)
      .pipe(finalize(() => this._spinner.display(false)))
      .subscribe(resp => {
          this.categories = resp['payload'];
        },
        error => {
          this._errorService.showErrorMessage(error);
        });
  }
}
