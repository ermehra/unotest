import {Component, OnInit} from '@angular/core';
import {UserLogin} from '../../models/user-login';
import {LoginService} from '../../services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {finalize} from 'rxjs/internal/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginModel: UserLogin = {
    user_name: '',
    password: ''
  };

  /*loginModel: UserLogin = {
    user_name: 'arun+icecream@unoapp.com',
    password: 'unoapp'
  };*/

  error: string;
  loading: boolean;
  redirectUrl: string;

  constructor(private _loginService: LoginService, private _router: Router, private _activatedRoute: ActivatedRoute) {
    // Return to home page in case already logged in.
    if (this._loginService.getLoggedUserValue()) {
      this._router.navigate(['']);
    }
  }

  ngOnInit() {
    this.redirectUrl = this._activatedRoute.snapshot.queryParams['url'] || '';
  }

  login() {
    this.error = null;
    this.loading = true;

    this._loginService.login(this.loginModel)
      .pipe(finalize(() => {
        this.loading = false;
      })).subscribe(resp => {
        console.log(resp);
        this._router.navigate([this.redirectUrl]);
      },
      error => {
        console.log('from login comp' + error);
        this.error = error;
      });
  }
}
