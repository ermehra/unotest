import {Component, OnInit} from '@angular/core';
import {LoaderService, LocationService} from '../../services';
import {OutletLocation} from '../../models';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/internal/operators';
import {ErrorService} from '../../services/error.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {

  constructor(private _locationService: LocationService,
              private _router: Router,
              private _loader: LoaderService,
              private _errorService: ErrorService) {
  }

  outletLocations: OutletLocation[];

  ngOnInit() {
    setTimeout(() => {
      this._loader.display(true);
      this._errorService.showErrorMessage(null);
    });

    this._locationService.getLocations()
      .pipe(finalize(() => this._loader.display(false)))
      .subscribe(resp => {
          if (resp) {
            this.outletLocations = resp.data.map((outlet) => {
              return new OutletLocation(outlet);
            });
          }
        },
        error => {
          this._errorService.showErrorMessage(error);
        });
  }

  navigateToMenu(id) {
    this._router.navigate(['menu', id]);
  }
}
