import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private _loginService: LoginService) {
  }

  ngOnInit() {
  }

  logout() {
    this._loginService.logout();
  }

}
