import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  errorMessageSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  errorObservable: Observable<string> = this.errorMessageSubject.asObservable();

  constructor() {
  }

  showErrorMessage(value: string) {
    this.errorMessageSubject.next(value);
  }
}
