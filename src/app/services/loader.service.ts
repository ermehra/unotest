import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loaderStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loaderObservable: Observable<boolean> = this.loaderStatus.asObservable();

  constructor() {
  }

  display(value: boolean) {
    this.loaderStatus.next(value);
  }
}
