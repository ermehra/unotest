import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API} from '../../environments/environment';
import {map} from 'rxjs/internal/operators';
import {Observable, throwError} from 'rxjs';
import {IPage, OutletLocation} from '../models';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private _http: HttpClient) {
  }

  getLocations() {

    const body = {
      'rpp': 10,
      'paginate': true,
      'brand_id': 25
    };

    return this._http.post<Observable<IPage>>(API.locations, body)
      .pipe(map(resp => {
        if (resp['payload']) {
          return resp['payload'];
        } else {
          throw new Error(resp['message']);
        }
      }));

  }
}
