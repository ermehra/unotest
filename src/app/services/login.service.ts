import {Injectable} from '@angular/core';
import {UserLogin} from '../models/user-login';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {API} from '../../environments/environment';
import {finalize, first, map} from 'rxjs/internal/operators';
import {UserKeys} from '../models/user-keys';
import {BehaviorSubject, Observable} from 'rxjs';
import {userError} from '@angular/compiler-cli/src/transformers/util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loggedUserSubject: BehaviorSubject<UserKeys> = new BehaviorSubject<UserKeys>(JSON.parse(localStorage.getItem('keys')));
  loggedUserObservable: Observable<UserKeys> = this.loggedUserSubject.asObservable();

  constructor(private _http: HttpClient) {
  }

  getLoggedUserValue(): UserKeys {
    if (this.isEmpty(this.loggedUserSubject.getValue())) {
      return null;
    } else {
      return new UserKeys(this.loggedUserSubject.getValue());
    }
  }

  login(loginInfo: UserLogin) {
    return this._http.post<any>(API.login, loginInfo)
      .pipe(map(response => {
          console.log(response);
          if (response.code === 200 && response.status.indexOf('ok') === 0) {

            /*
            User successfully logged in
            Store keys localStorage for handling browser refresh
            */
            const userKeys: UserKeys = new UserKeys({
              'appId': response.App.app_id,
              'appSecret': response.App.app_secret,
              'authToken': response.session.token
            });

            localStorage.setItem('keys', JSON.stringify(userKeys));
            this.loggedUserSubject.next(userKeys);
          }
          return response;
        }
      ));
  }

  logout() {
    const httpOptions = {
      headers: new HttpHeaders({
        'auth-token': this.getLoggedUserValue().authToken
      })
    };
    this._http.get(API.logout, httpOptions)
      .pipe(finalize(() => {
        localStorage.removeItem('keys');
        this.loggedUserSubject.next(null);
        location.reload(true);
      }))
      .subscribe();
  }

  isEmpty(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
}
