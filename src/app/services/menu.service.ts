import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API} from '../../environments/environment';
import {map} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private _http: HttpClient) {
  }

  getMenu(id) {
    return this._http.get(API.menu + id);
      /*.pipe(map(resp => {
        return resp['payload'];
      }));*/
  }

}
