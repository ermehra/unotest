import {Component} from '@angular/core';
import {UserKeys} from './models';
import {LoaderService, LoginService} from './services';
import {ErrorService} from './services/error.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loggedIn: UserKeys;
  showloader: boolean;
  errorMessage: string;

  constructor(private _loginService: LoginService, private _loader: LoaderService, private _errorService: ErrorService) {
    this._loginService.loggedUserObservable.subscribe(resp => this.loggedIn = resp);
    this._loader.loaderObservable.subscribe(resp => this.showloader = resp);
    this._errorService.errorObservable.subscribe((msg => this.errorMessage = msg));
  }
}
