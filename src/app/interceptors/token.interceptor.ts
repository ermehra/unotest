import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginService} from '../services';
import {UserKeys} from '../models';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private _loginService: LoginService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const keys: UserKeys = this._loginService.getLoggedUserValue();
    if (keys) {
      req = req.clone({
        setHeaders: {'app-id': keys.appId, 'app-secret': keys.appSecret}
      });
    }

    return next.handle(req);
  }

}
