import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/internal/operators';
import {LoginService} from '../services/login.service';
import {Injectable} from '@angular/core';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private _loginService: LoginService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Handling http errors
    return next.handle(req).pipe(catchError((error) => {
      // If this is a unauthorized access logout user
      if (error.status === 401) {
        this._loginService.logout();
        location.reload(true);
      }

      console.log('Error Interceptor');
      console.log(error);

      const errorMsg = error.error.message || error.message;
      return throwError(errorMsg);
    }));
  }

}
