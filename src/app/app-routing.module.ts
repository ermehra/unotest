import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LocationsComponent, LoginComponent, MenuComponent, PageNotFoundComponent} from './components';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  {path: '', component: LocationsComponent, canActivate: [AuthGuard]},
  {path: 'locations', component: LocationsComponent, canActivate: [AuthGuard]},
  {path: 'menu/:id', component: MenuComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const RoutingComponents = [LoginComponent, PageNotFoundComponent, LocationsComponent, MenuComponent];
