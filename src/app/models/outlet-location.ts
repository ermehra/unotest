export class OutletLocation {
  id: number;
  display_name: string;
  address_line_1: string;
  address_line_2: string;
  city: string;
  province: string;
  postal_code: string;
  country: string;

  constructor(obj) {
    Object.assign(this, obj);
  }

  getAddress() {
    let address = '';

    address = this.address_line_1 ? this.address_line_1 : '';
    address += this.address_line_2 ? ', ' + this.address_line_2 : '';

    address += this.city ? '\n' + this.city : '';
    address += this.province ? ', ' + this.province : '';


    return address;
  }
}
