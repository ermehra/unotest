export class UserKeys {
  appId: string;
  appSecret: string;
  authToken: string;

  constructor(obj) {
    Object.assign(this, obj);
  }

  getAppIdAndSecret() {
    return {'app-id': this.appId, 'app-secret': this.appSecret};
  }
}
