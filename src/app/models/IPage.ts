import {OutletLocation} from './outlet-location';

export class IPage {
  total: number;
  per_page: number;
  current_page: number;
  last_page: number;
  from: number;
  to: number;
  data: OutletLocation[];
}
