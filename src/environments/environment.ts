// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

const BASE_URL = 'https://multivendor.dev.api.unoapp.io/'

export const API = {
  login: `${BASE_URL}api/application/admin/login`,
  locations: `${BASE_URL}api/app/locations/all`,
  menu: `${BASE_URL}api/app/menus/full/`,
  logout: `${BASE_URL}api/application/admin/logout`
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
