export const environment = {
  production: true,
};

const BASE_URL = 'https://multivendor.dev.api.unoapp.io/'

export const API = {
  login: `${BASE_URL}api/application/admin/login`,
  locations: `${BASE_URL}api/app/locations/all`,
  menu: `${BASE_URL}api/app/menus/full/`,
  logout: `${BASE_URL}api/application/admin/logout`
}
